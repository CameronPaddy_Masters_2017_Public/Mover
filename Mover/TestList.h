#pragma once

typedef struct ListNode {
	TCHAR *filename;
	struct ListNode* next;
} ListNode;

typedef struct List {
	ListNode *head;
	int size;
} List;

void dispose_list(List *list); //clear the contents of a list
void init_list(List *list); //initalise a list for use
int list_size(List *list); //return the size of the a list
void prepend_list(List *list, TCHAR *filename); //add item to the start of the list
void remove_any(List *list, ListNode* nd); //remove the node nd from the lost
void remove_back(List *list); //remove the tail of the lost
void remove_front(List *list); //remove the from of the list
void remove_index(List *list, int index); //remove the list at the index provided
ListNode *select_node(List *list, int index); //give an index in the list return a pointer to the node with that index
