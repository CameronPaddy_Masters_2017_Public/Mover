// Mover.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "TestList.h"
#include "csprng.h"

void ListDirectoryFiles(TCHAR *directory, List* list) {
	HANDLE handle;
	WIN32_FIND_DATA file;

	TCHAR searchString[100];
	wcscpy(searchString, directory);
	wcscat(searchString, L"*");
	TCHAR filepath[MAX_PATH];

	handle = FindFirstFile(searchString, &file);
	searchString[wcslen(searchString) - 1] = 0;

	if (handle != INVALID_HANDLE_VALUE) {
		FindNextFile(handle, &file);
		while (FindNextFile(handle, &file) != 0) {
			wcscpy(filepath, directory);
			wcscat(filepath, file.cFileName);
			prepend_list(list, &filepath);
		}
	}
}

TCHAR *randstring(size_t length, BOOL truRand) {
	static char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	TCHAR *randomString = NULL;
	CSPRNG rng2 = NULL;
	rng2 = csprng_create(rng2);
	if (!rng2)
		return 1;

	if (length) {
		randomString = malloc(sizeof(TCHAR) * (length + 1) + 10);
		if (randomString) {
			for (int n = 0; n < length; n++) {
				int key;
				if (truRand == TRUE) {
					key = csprng_get_int(rng2);
					key %= (int)(sizeof(charset) - 1);
				}
				else
					key = rand() % (int)(sizeof(charset) - 1);

				randomString[n] = charset[key];
			}
			randomString[length] = '\0';
		}
	}
	return randomString;
}



int main(int argc, char *argv[])
{	
	int status;
	long msPause = 0;
	List testList;
	CSPRNG rng = NULL;
	init_list(&testList);
	unsigned long randomDelay = 0;
	BOOL truRand = FALSE;
	TCHAR *random;

	if (argc == 2) {
		errno = 0;
		msPause = strtol(argv[1], NULL, 10);
		if (errno != 0) {
			printf("Error! Please enter only number values in ms.");
			return -1;
		}
	}

	if (msPause > 20000)
		msPause = 0;

	rng = csprng_create(rng);
	if (!rng)
		return 1;

	TCHAR base[100] = L"C:\\ProggerTests";
	status = CreateDirectoryW(base, NULL);
	wcscat(base, L"\\Mover");
	status = CreateDirectoryW(base, NULL);
	wcscat(base, L"\\");
	ListDirectoryFiles(base, &testList);

	for (int i = 0; i < 20000; i++) {
		if (msPause == -1) {
			randomDelay = csprng_get_int(rng);
			randomDelay %= 5001;
			Sleep(randomDelay);
			truRand = TRUE;
		}
		else
			Sleep(msPause);

		TCHAR toUse[MAX_PATH], toRename[MAX_PATH];
		char usablePath[MAX_PATH], newPath[MAX_PATH];

		int myInt = rand() % list_size(&testList);
		ListNode *tmp = select_node(&testList, myInt);
		wcscpy(toUse, tmp->filename);
		random = randstring(80, FALSE);
		wcscpy(toRename, base);
		wcscat(toRename, random);
		wcscat(toRename, L".txt");
		
		wcstombs(usablePath, toUse, MAX_PATH);
		wcstombs(newPath, toRename, MAX_PATH);
		rename(usablePath, newPath);
		wcscpy(tmp->filename, toRename);
		free(random);
	}

    return 0;
}

